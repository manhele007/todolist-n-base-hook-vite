import React from "react";
import Routers from "./router";
import "./App.css";
import { Outlet } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store/index";

export default function App() {
  return (
    <Provider store={store} className="App">
      <Routers />
      {/* <Outlet /> */}
    </Provider>
  );
}
