import { REST_API_METHOD as METHOD, BASE_URL } from '../constant'

const API_SCHEME = {
    // update when apply real authen api
    AUTHEN: {
        VALIDATE_TOKEN: {
            url: `${BASE_URL}/auth`,
            method: METHOD.GET
        },
        LOGIN: {
            url: `${BASE_URL}/auth`,
            method: METHOD.GET
        },
        LOGOUT: {
            url: `${BASE_URL}/logout`,
            method: METHOD.POST
        }
    },
    // business api
    JOB: {
        GET_JOB: {
            url: `${BASE_URL}/Job/get`,
            method: METHOD.GET
        },
        CREATE_JOB: {
            url: `${BASE_URL}/Job/add`,
            method: METHOD.POST
        },
        UPDATE_JOB: {
            url: `${BASE_URL}/Job/update/:id`,
            method: METHOD.PUT
        },
        DELETE_JOB: {
            url: `${BASE_URL}/Job/delete/:id`,
            method: METHOD.DELETE
        },
        SEARCH_JOB: {
            url: `${BASE_URL}/Job`,
            method: METHOD.GET
        },
    },

    JOBDETAIL: {
        GET_JOBDETAIL: {
            url: `${BASE_URL}/Job/getJobDetails/:id`,
            method: METHOD.GET
        },

        CREATE_JOBDETAIL: {
            url: `${BASE_URL}/jobDetails/post/:idJob`,
            method: METHOD.POST
        },
        UPDATE_JOBDETAIL: {
            url: `${BASE_URL}/jobDetails/put/:idJobDetails`,
            method: METHOD.PUT
        },
        DELETE_JOBDETAIL: {
            url: `${BASE_URL}/jobDetails/delete/:id`,
            method: METHOD.DELETE
        },
        SEARCH_JOBDETAIL: {
            url: `${BASE_URL}/jobDetails`,
            method: METHOD.GET
        },
    },

    STATUS: {
        GET_STATUS: {
            url: `${BASE_URL}/posts`,
            method: METHOD.GET
        },
        GET_DETAIL_STATUS: {
            url: `${BASE_URL}/status/:id`,
            method: METHOD.GET
        },
        CREATE_STATUS: {
            url: `${BASE_URL}/status`,
            method: METHOD.POST
        },
        UPDATE_STATUS: {
            url: `${BASE_URL}/status/:id`,
            method: METHOD.PUT
        },
        DELETE_STATUS: {
            url: `${BASE_URL}/status/:id`,
            method: METHOD.DELETE
        },
        SEARCH_STATUS: {
            url: `${BASE_URL}/posts`,
            method: METHOD.GET
        },
    },


}

export default API_SCHEME