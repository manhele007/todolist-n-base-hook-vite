import React from "react";
import { Avatar, Layout, Typography } from "antd";
import Sidebar from "./Sidebar";
import { UserOutlined } from "@ant-design/icons";
import { Outlet } from "react-router-dom";

const { Content, Sider } = Layout;

function LayoutDefault() {
  return (
    <Layout>
      <Sider>
        <div style={{ textAlign: "center", margin: "15px 0" }}>
          <Avatar size={100} icon={<UserOutlined />} />
        </div>
        <Sidebar />
      </Sider>
      <Content>
        <Outlet />
      </Content>
    </Layout>
  );
}
export default LayoutDefault;
