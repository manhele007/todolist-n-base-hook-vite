import React, { useEffect, useState } from "react";
import { Button, Col, Input, Modal, Row, Select, Typography } from "antd";

import { useJobDetail, useStatus } from "../../hooks";

function ModalCreateJobDetail(props) {
    const { Title } = Typography;

    const { handleCreateJobDetail, handleUpdateJobDetail } = useJobDetail();

    const { handleGetStatus, listStatus } = useStatus();

    const [nameJobDetails, setNameUpdate] = useState();

    const { isOpen, cancelModalCreate, itemUpdate, itemUpdateDetail } = props;

    const handleCancel = () => {
        cancelModalCreate();
    };

    const createJobDetail = () => {
        handleCreateJobDetail({ idJob: itemUpdate.id, nameJobDetails });
        setNameUpdate("");
        handleCancel();
    };

    const updateJobDetail = () => {
        handleUpdateJobDetail({ id: itemUpdate.id, nameJobDetails, idJob: itemUpdateDetail.id });
        setNameUpdate("");
        handleCancel();
    };
    const onChange = (value) => {
        console.log(`selected ${value}`);
    };
    const onSearch = (value) => {
        console.log('search:', value);
    };
    useEffect(() => {
        setNameUpdate(itemUpdate?.nameJobDetails);
        handleGetStatus()
    }, [itemUpdate]);

    const titleColor = itemUpdate?.nameJobDetails ? "#1677ff" : "green";
    const submit = itemUpdate?.nameJobDetails ? "#1677ff" : "green";

    return (
        <Modal destroyOnClose={true}
            open={isOpen}
            onCancel={handleCancel}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button
                    style={{ backgroundColor: submit, color: "white" }}
                    onClick={() => {
                        if (itemUpdate?.nameJobDetails) {
                            updateJobDetail();
                        } else {
                            createJobDetail();
                        }
                    }}
                > Submit
                </Button>,
            ]}
        >
            <div className="modalBackground">
                <Title level={2} style={{ color: titleColor, marginBottom: "8%" }}>
                    {itemUpdate?.nameJobDetails ? "Sửa Công Việc" : "Thêm Công Việc"}
                </Title>
                <Row>
                    <Col span={8}>
                        <Title level={4}>Tên công việc:</Title>
                    </Col>
                    <Col span={16}>
                        <Input
                            type="text"
                            value={nameJobDetails}
                            onChange={(e) => {
                                setNameUpdate(e.target.value);
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col span={8}>
                        <Title level={4}>Trạng thái:</Title>
                    </Col>
                    <Col span={16}>
                        <Select size={"large"}
                            placeholder="Sắp xếp"
                            optionFilterProp="children"
                            onChange={onChange}
                            onSearch={onSearch}
                            // filterOption={(input, option) =>
                            //     (option?.lable ?? '').toLowerCase().includes(input.toLowerCase())
                            // }
                            filterOption={(input, options) =>
                                (options?.label ?? '').toLowerCase().includes(input.toLowerCase())
                            }
                            options={listStatus}
                        />
                    </Col>
                </Row>
            </div>
        </Modal>
    );
}

export default ModalCreateJobDetail;
