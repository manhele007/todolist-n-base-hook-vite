import React, { useEffect, useState }  from "react";
import { Button, Modal, Typography } from "antd";
import { WarningOutlined } from "@ant-design/icons";

import { useJobDetail } from "../../hooks";
import "../../styles/Job.css";

function ModalDeleteJobDetail(props) {

    const { Title } = Typography;


    const { handleDeleteJobDetail, isFetching, isError } = useJobDetail();

    const [idDel, setNameUpdate] = useState();

    const { isOpen, cancelModalDelete, itemDel,itemDetailDel } = props;
    const handleCancel = () => {
        cancelModalDelete()
    };
    const DeleteJobDetail = () => {
        handleDeleteJobDetail({itemDel, idDel});
        handleCancel();
    };
    useEffect(() => {
        setNameUpdate(itemDetailDel?.id);
    }, [itemDetailDel]);
    return (
        <Modal
            open={isOpen}
            onCancel={handleCancel}
            footer={[
                <Button key="back" onClick={handleCancel}>
                    Cancel
                </Button>,
                <Button
                    style={{ backgroundColor: "red", color: "white" }}
                    onClick={() => {
                        DeleteJobDetail();
                    }}
                > Delete
                </Button>,
            ]}
        >
            <div className="modalDel">
                <WarningOutlined className="icon-warning" />
                <Title level={2} style={{ color: "red", marginBottom: "8%" }}>
                    Bạn có muốn xoá không ?
                </Title>
            </div>
            <div>
                <Title level={4} style={{ marginBottom: '5%' }}>
                    Bạn có chắc chắn muốn xoá
                    <span style={{ color: "red" }}> "{itemDel?.nameJobDetails}" </span> không ?
                </Title>
            </div>
        </Modal>
    );
}

export default ModalDeleteJobDetail;
