import React, { useState } from "react";
import { Menu } from "antd";
import { NavLink } from "react-router-dom";

function Sidebar() {
  const [current, setCurrent] = useState('1');
  const onClick = (e) => {
    setCurrent(e.key);
  };
  const items = [
    {
      label: <NavLink to={"/"}> Quản lý công việc</NavLink>,
      key: '1'
    },
    {
      label: <NavLink to={"/status"}> Trạng thái</NavLink>,
      key: '2'
    }
  ];
  return (
    <>
      <Menu
        onClick={onClick}
        style={{
          width: 256,
          height: "100vh"
        }}
        openKeys={['1']}
        selectedKeys={[current]}
        mode="vertical"
        theme="dark"
        items={items}
        // getPopupContainer={function test(node) {
        //   return node.parentNode;
        // }}
      />
    </>
  );
}

export default Sidebar;
