import { useDispatch, useSelector } from "react-redux"
import { JobDetailAction } from '../actions'

export const useJobDetail = () => {
    const dispatch = useDispatch()

    const listJobDetail = useSelector(state => state.jobDetailCollection.listJobDetail)
    const textSearch = useSelector(state => state.jobDetailCollection.textSearch)

    const isFetching = useSelector(state => state.jobDetailCollection.isFetching)
    const isError = useSelector(state => state.jobDetailCollection.isError)
    const message = useSelector(state => state.jobDetailCollection.message)

    const handleGetJobDetail = (data) => dispatch(JobDetailAction.getJobDetailRequest(data))
    const handleCreateJobDetail = (data) => dispatch(JobDetailAction.createJobDetailRequest(data))
    const handleDeleteJobDetail = (data) => dispatch(JobDetailAction.deleteJobDetailRequest(data))
    const handleUpdateJobDetail = (data) => dispatch(JobDetailAction.updateJobDetailRequest(data))
    const handleSearchJobDetail = (data) => dispatch(JobDetailAction.searchJobDetailRequest(data))

    return {
        listJobDetail,
        isFetching,
        isError,
        message,
        handleGetJobDetail,
        handleCreateJobDetail,
        handleDeleteJobDetail,
        handleUpdateJobDetail,
        handleSearchJobDetail,
        textSearch
    }
}