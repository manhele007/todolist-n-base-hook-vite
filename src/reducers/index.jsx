import { combineReducers } from 'redux'
import jobCollectionReducer from './jobReducer/job.collection.reducer'
import jobInstanceReducer from './jobReducer/job.instance.reducer'
import jobDetailCollectionReducer from './jobDetailReducer/jobDetail.collection.reducer'
import jobDetailInstanceReducer from './jobDetailReducer/jobDetail.instance.reducer'
import statusCollectionReducer from './statusReducer/status.collection.reducer'
import statusInstanceReducer from './statusReducer/status.instance.reducer'

export default combineReducers({
    jobCollection: jobCollectionReducer,
    jobInstance: jobInstanceReducer,
    jobDetailCollection: jobDetailCollectionReducer,
    jobDetailInstance: jobDetailInstanceReducer,
    statusCollection: statusCollectionReducer,
    statusInstance: statusInstanceReducer,
})