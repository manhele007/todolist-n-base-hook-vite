import { actionTypes } from "../../constant";

const { JobDetailTypes } = actionTypes;
const INITIAL_STATE = {
    listJobDetail: [],
    textSearch: "",
    isFetching: false,
    isError: false,
    message: "",
};

export default function jobDetailCollectionReducer(
    state = INITIAL_STATE,
    { type, payload }
) {
    switch (type) {
        case JobDetailTypes.GET_JOB_DETAIL_REQUEST:
            return {
                ...state,
                isFetching: true,
                isError: false,
                message: "",
            };

        case JobDetailTypes.GET_JOB_DETAIL_SUCCESS:
            return {
                ...state,
                isFetching: false,
                listJobDetail: payload.listJobDetail,
                isError: false,
            };

        case JobDetailTypes.GET_JOB_DETAIL_FAILURE:
            return {
                ...state,
                isFetching: false,
                isError: true,
                message: payload.message,
            };

        case JobDetailTypes.SEARCH_JOB_DETAIL_REQUEST:
            return {
                ...state,
                isFetching: true,
                isError: false,
                message: "",
            };

        case JobDetailTypes.SEARCH_JOB_DETAIL_SUCCESS:
            return {
                ...state,
                isFetching: false,
                listJob: payload.listJob,
                textSearch: payload.textSearch,
                isError: false,
            };

        case JobDetailTypes.SEARCH_JOB_DETAIL_FAILURE:
            return {
                ...state,
                isFetching: false,
                isError: true,
                message: payload.message,
            };
        default:
            return state;
    }
}
