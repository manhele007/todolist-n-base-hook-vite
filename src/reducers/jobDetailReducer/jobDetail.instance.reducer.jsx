import { actionTypes } from "../../constant";

const { JobDetailTypes } = actionTypes;
const INITIAL_STATE = {
    isFetching: false,
    isError: false,
    message: "",
};

export default function jobInstanceReducer(
    state = INITIAL_STATE,
    { type, payload }
) {
    switch (type) {
        case JobDetailTypes.CREATE_JOB_DETAIL_REQUEST:
        case JobDetailTypes.DELETE_JOB_DETAIL_REQUEST:
        case JobDetailTypes.UPDATE_JOB_DETAIL_REQUEST:
            return {
                ...state,
                isFetching: true,
                isError: false,
                message: "",
            };

        case JobDetailTypes.CREATE_JOB_DETAIL_FAILURE:
        case JobDetailTypes.DELETE_JOB_DETAIL_FAILURE:
        case JobDetailTypes.UPDATE_JOB_DETAIL_FAILURE:

            return {
                ...state,
                isFetching: false,
                isError: true,
                message: payload.message,
            };

        case JobDetailTypes.CREATE_JOB_DETAIL_SUCCESS:
        case JobDetailTypes.DELETE_JOB_DETAIL_SUCCESS:
        case JobDetailTypes.UPDATE_JOB_DETAIL_SUCCESS:
            return {
                ...state,
                isFetching: false,
                message: payload.res,
            };
        case JobDetailTypes:
            return INITIAL_STATE;
        default:
            return state;
    }
}
