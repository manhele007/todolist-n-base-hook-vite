import { BrowserRouter, Routes, Route } from "react-router-dom";
import * as view from "../view";
import LayoutDefault from "../components/Layout";

export default function Navbar() {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<LayoutDefault />}>
          <Route path="/" element={<view.Job />} />
          <Route path="/status" element={<view.Status />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
