import { takeLatest, put, takeEvery } from 'redux-saga/effects'
import { JobDetailAction } from '../actions'
import { actionTypes } from '../constant'
import { jobDetailApi } from '../api'

function* handleJobDetailRequest({ payload }) {
    try {
        const res = yield jobDetailApi.getJobDetail({ id: payload }, null, null)
        yield put(JobDetailAction.getJobDetailSuccess({
            listJobDetail: res.jobDetails,
        }))
    } catch (error) {
        yield put(JobDetailAction.getJobDetailFailure({
            message: error.message
        }))
    }
}


function* handleCreateJobDetail({ payload }) {
    try {
        const id = payload.idJob
        const res = yield jobDetailApi.createJobDetail({ idJob: payload.idJob }, null, { nameJobDetails: payload.nameJobDetails })
        yield put(JobDetailAction.createJobDetailSuccess(res))
        yield put(JobDetailAction.getJobDetailRequest(id, null, null))
    } catch (error) {
        yield put(JobDetailAction.createJobDetailFailure({
            message: error.message
        }))
    }
}

function* handleDeleteJobDetail({ payload }) {
    const id = payload.idDel
    try {
        const res = yield jobDetailApi.deleteJobDetail({ id: payload.itemDel.id }, null, null)
        yield put(JobDetailAction.deleteJobDetailSuccess(res))
        yield put(JobDetailAction.getJobDetailRequest(id))
    } catch (error) {
        yield put(JobDetailAction.deleteJobDetailFailure({
            message: error.message
        }))
    }
}


function* handleUpdateJobDetail({ payload }) {
    console.log(payload, "Đây là update saga");

    try {
        const id = payload.idJob
        const res = yield jobDetailApi.updateJobDetail({ idJobDetails: payload.id }, null, { nameJobDetails: payload.nameJobDetails })
        yield put(JobDetailAction.updateJobDetailSuccess(res))
        yield put(JobDetailAction.getJobDetailRequest(id))
    } catch (error) {
        alert(error)
        yield put(JobDetailAction.updateJobDetailFailure({
            message: error.message
        }))
    }
}

// function* handleSearchJob({ payload }) {
//     try {
//         const res = yield jobApi.searchJob()
//         yield put(JobAction.searchJobSuccess({
//             list: res.listJob,
//         }))
//     } catch (error) {
//         yield put(JobAction.searchJobFailure({
//             message: error.message
//         }))
//     }
// }

const JobSaga = [
    takeLatest(actionTypes.JobDetailTypes.GET_JOB_DETAIL_REQUEST, handleJobDetailRequest),
    takeLatest(actionTypes.JobDetailTypes.CREATE_JOB_DETAIL_REQUEST, handleCreateJobDetail),
    takeLatest(actionTypes.JobDetailTypes.DELETE_JOB_DETAIL_REQUEST, handleDeleteJobDetail),
    takeLatest(actionTypes.JobDetailTypes.UPDATE_JOB_DETAIL_REQUEST, handleUpdateJobDetail),
    // takeEvery(actionTypes.JobTypes.SEARCH_JOB_REQUEST, handleSearchJob),
]

export default JobSaga;