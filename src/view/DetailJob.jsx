import React, { useEffect, useState } from "react";
import { Button, Col, Input, Row, Select, Space, Table, Typography, Modal } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

import { useJobDetail, useStatus } from "../hooks/index";
import ModalCreateJobDetail from "../components/ModalDetailJob/CreateJobDetail";
import ModalDeleteJobDetail from "../components/ModalDetailJob/DeleteJobDetail";

export default function GetDetailJob(props) {
  const { Title } = Typography;

  const [nameJobDetails, setNameUpdate] = useState();

  const { isOpen, cancelModalJobDetail, itemDetailUpdate } = props;

  const handleCancel = () => {
    cancelModalJobDetail();
  };


  const { listJobDetail, handleGetJobDetail, handleSearchJobDetail } = useJobDetail();

  const [isVisible, setIsVisible] = useState(false);

  const [isShown, setIsShown] = useState(false);

  const [textSearch, setTextSearch] = useState("");

  const [itemDel, setItemDel] = useState();

  const [itemUpdate, setItemUpdate] = useState()

  const [itemDetailDel, setItemDetailDel] = useState()

  const showModalCreate = (record) => {
    setIsVisible(true);
    setItemUpdate(record);

  };

  const cancelModalCreate = () => {
    setIsVisible(false);
  };

  const showModalDelete = (record) => {
    setIsShown(true);
    setItemDel(record);
  };

  const cancelModalDelete = () => {
    setIsShown(false);
  };

  useEffect(() => {
    handleGetJobDetail(itemDetailUpdate?.id);
  }, [itemDetailUpdate]);

  const columns = [
    {
      title: "Tên công việc",
      dataIndex: "nameJobDetails",
      key: "nameJobDetails",
    },
    {
      title: "Trạng thái",
      dataIndex: "progress",
      key: "progress",
      align: "center",
    },
    {
      title: "Action",
      key: "x",
      align: "center",
      render: (_, record) => (
        <Space size={"middle"}>
          <Button
            type="primary"
            icon={<EditOutlined />}
            onClick={() => {
              showModalCreate(record)
            }}
            style={{ cursor: 'pointer' }}
          />

          <Button
            danger
            type="primary"
            icon={<DeleteOutlined />}
            onClick={() => {
              showModalDelete(record);
            }}
            style={{ cursor: "pointer" }}
          />
        </Space>
      ),
    },
  ];


  return (
    <Modal
      open={isOpen}
      onCancel={handleCancel}
      footer={null}
      width={"70%"}
    >
      <div className="main">
        <ModalDeleteJobDetail
          isOpen={isShown}
          cancelModalDelete={cancelModalDelete}
          itemDel={itemDel}
          itemDetailDel={itemDetailUpdate}

        />
        <ModalCreateJobDetail
          isOpen={isVisible}
          cancelModalCreate={cancelModalCreate}
          itemUpdate={itemUpdate}
          itemUpdateDetail={itemDetailUpdate}
        />
        <div className="title">
          <Title>{itemDetailUpdate?.nameJob}</Title>
        </div>
        <div className="menu-search">
          <Row>
            <Col span={18}>
              <Row>
                <Col span={18}>
                  <Input
                    size="large"
                    placeholder="Nhập để tìm kiếm..."
                    onChange={(e) => {
                      setTextSearch(e.target.value);
                    }}
                  />
                </Col>
                <Col span={4} offset={2}>
                  <Button
                    size={"large"}
                    style={{ backgroundColor: "orange" }}
                    onClick={() => {
                      handleSearchJobDetail({ name: textSearch });
                    }}
                  >
                    Search
                  </Button>
                </Col>
              </Row>
            </Col>

            <Col span={6}>
              <Row>
                <Col span={12}>
                  <Button
                    size={"large"}
                    style={{ backgroundColor: "green", color: "white" }}
                    onClick={() => {
                      showModalCreate(itemDetailUpdate);
                    }}
                  >
                    Create
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>

        <div>
          <Table
            onRow={(record) => {
              return {
                onClick: () => {
                  console.log(record);
                },
              };
            }}
            columns={columns}
            scroll={{
              y: 300,
            }}
            scrollToFirstRowOnChange={true}
            dataSource={listJobDetail}
            rowKey={(key) => key._id}
          ></Table>
        </div>
      </div>
    </Modal>
  );
}
