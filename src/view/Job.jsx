import React, { useEffect, useState } from "react";
import { Button, Col, Input, Row, Select, Space, Table, Typography, message } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

import { useJob } from "../hooks";
import ModalCreateJob from "../components/ModalJob/CreateJob";
import ModalDeleteJob from "../components/ModalJob/DeleteJob";

import "../styles/Job.css";
import GetDetailJob from "./DetailJob";

const { Title } = Typography;

export default function Status() {

  const { listJob, handleGetJob, handleSearchJob, messagee } = useJob();

  const [isVisible, setIsVisible] = useState(false);

  const [isShown, setIsShown] = useState(false);

  const [showDetail, setshowDetail] = useState(false);

  const [textSearch, setTextSearch] = useState("");

  const [itemDel, setItemDel] = useState();

  const [itemUpdate, setItemUpdate] = useState()
  const [itemDetailUpdate, setItemDetailUpdate] = useState()
  const showModalCreate = (record) => {
    setIsVisible(true);
    setItemUpdate(record);
  };

  const cancelModalCreate = () => {
    setIsVisible(false);
  };

  const showModalDelete = (record) => {
    setIsShown(true);
    setItemDel(record);
  };

  const cancelModalDelete = () => {
    setIsShown(false);
  };

  const showModalDetail = (record) => {
    setItemDetailUpdate(record)
    setshowDetail(true)

  }
  const cancelModalJobDetail = () => {
    setshowDetail(false);
  };
  const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  const onSearch = (value) => {
    console.log('search:', value);
  };

  useEffect(() => {
    handleGetJob();
  }, []);

  const columns = [
    {
      title: "Tên công việc",
      dataIndex: "nameJob",
      key: "nameJob",
      render: (_, record) => (
        <Space size={"middle"}>
          <Button
            type="link"
            onClick={() => {
              showModalDetail(record)
            }}
          >{record.nameJob}</Button>
        </Space>
      ),
    },
    {
      title: "Tiến độ công việc",
      dataIndex: "progress",
      key: "progress",
      align: "center",
    },
    {
      title: "Action",
      key: "Action",
      align: "center",
      render: (_, record) => (
        <Space size={"middle"}>
          <Button
            type="primary"
            icon={<EditOutlined />}
            onClick={() => {
              showModalCreate(record)
            }}
            style={{ cursor: 'pointer' }}
          />

          <Button
            danger
            type="primary"
            icon={<DeleteOutlined />}
            onClick={() => {
              showModalDelete(record);
            }}
            style={{ cursor: "pointer" }}
          />
        </Space>
      ),
    },
  ];

  return (
    <div className="main">
      <ModalDeleteJob
        isOpen={isShown}
        cancelModalDelete={cancelModalDelete}
        itemDel={itemDel}
      />
      <ModalCreateJob
        isOpen={isVisible}
        cancelModalCreate={cancelModalCreate}
        itemUpdate={itemUpdate}
      />
      <GetDetailJob
        isOpen={showDetail}
        cancelModalJobDetail={cancelModalJobDetail}
        itemDetailUpdate={itemDetailUpdate}
      />
      <div className="title">
        <Title>Danh sách công việc</Title>
      </div>

      <div className="menu-search">
        <Row>
          <Col span={18}>
            <Row>
              <Col span={18}>
                <Input
                  size="large"
                  placeholder="Nhập để tìm kiếm..."
                  onChange={(e) => {
                    setTextSearch(e.target.value);
                  }}
                />
              </Col>
              <Col span={4} offset={2}>
                <Button
                  size={"large"}
                  style={{ backgroundColor: "orange" }}
                  onClick={() => {
                    handleSearchJob({ name: textSearch });
                  }}
                >
                  Search
                </Button>
              </Col>
            </Row>
          </Col>

          <Col span={6}>
            <Row>
              <Col span={12}>
                <Button
                  size={"large"}
                  style={{ backgroundColor: "green", color: "white" }}
                  onClick={() => {
                    showModalCreate();
                  }}
                >
                  Create
                </Button>
              </Col>
              <Col span={10} offset={2}>
                <Select size={"large"} placeholder="Sắp xếp"
                  optionFilterProp="children"
                  onChange={onChange}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  options={[
                    {
                      value: '1',
                      label: 'Từ lớn đến bé',
                    },
                    {
                      value: '2',
                      label: 'Từ bé đến lớn',
                    },
                  ]}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>

      <div>
        <Table
          columns={columns}
          dataSource={listJob}
          scroll={{
            // x: 1500,
            y: 454,
          }}
          scrollToFirstRowOnChange={true}
        // rowKey={(key) => key._id}
        ></Table>
      </div>
    </div>
  );
}
