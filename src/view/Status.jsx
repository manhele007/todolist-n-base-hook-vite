import React, { useEffect, useState } from "react";
import { Button, Col, Input, Row, Select, Space, Table, Typography } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

import { useStatus } from "../hooks";
import ModalCreateStatus from "../components/ModalStatus/CreateStatus";
import ModalDeleteStatus from "../components/ModalStatus/DeleteStatus";

import "../styles/Status.css";

const { Title } = Typography;

export default function Status() {
  const { listStatus, handleSearchStatus, handleGetStatus, activePage,
    totalPage } = useStatus();

  const [isVisible, setIsVisible] = useState(false);

  const [isShown, setIsShown] = useState(false);

  const [textSearch, setTextSearch] = useState('');

  const [itemDel, setItemDel] = useState();

  const [itemUpdate, setItemUpdate] = useState()

  const showModalCreate = (record) => {
    setIsVisible(true);
    setItemUpdate(record);
  };

  const cancelModalCreate = () => {
    setIsVisible(false);
  };

  const showModalDelete = (record) => {
    setIsShown(true);
    setItemDel(record);
  };

  const cancelModalDelete = () => {
    setIsShown(false);
  };
  const onChange = (value) => {
    console.log(`selected ${value}`);
  };
  const onSearch = (value) => {
    console.log('search:', value);
  };
  useEffect(() => {
    handleGetStatus(1);
  }, []);

  const arr = []
  for (let i = 1; i <= totalPage; i++) {
    arr.push(i)
  }
  let listItems = listStatus.map((items, key) => {
    return (
      <tr key={key}>
        <th>{key + 1}</th>
        <th>{items.title}</th>

      </tr>
    )
  })
  // const columns = [
  //   {
  //     title: "Trạng thái",
  //     dataIndex: "title",
  //     key: "status",
  //   },
  //   {
  //     title: "Action",
  //     dataIndex: "",
  //     key: "x",
  //     align: "center",
  //     render: (_, record) => (
  //       <Space size={"middle"}>
  //         <Button
  //           type="primary"
  //           icon={<EditOutlined />}
  //           onClick={() => {
  //             showModalCreate(record)
  //           }}
  //           style={{ cursor: 'pointer' }}
  //         />

  //         <Button
  //           danger
  //           type="primary"
  //           icon={<DeleteOutlined />}
  //           onClick={() => {
  //             showModalDelete(record);
  //           }}
  //           style={{ cursor: "pointer" }}
  //         />
  //       </Space>
  //     ),
  //   },

  // ];


  return (
    <div className="main">
      {/* <ModalDeleteStatus
        isOpen={isShown}
        cancelModalDelete={cancelModalDelete}
        itemDel={itemDel}
      /> */}
      {/* <ModalCreateStatus
        isOpen={isVisible}
        cancelModalCreate={cancelModalCreate}
        itemUpdate={itemUpdate}
      /> */}
      <div className="title">
        <Title>Danh sách trạng thái</Title>
      </div>

      <div className="menu-search">
        <Row>
          <Col span={18}>
            <Row>
              <Col span={18}>
                <Input
                  size="large"
                  placeholder="Nhập để tìm kiếm..."
                  onChange={(e) => {
                    setTextSearch(e.target.value);
                  }}
                />
              </Col>
              <Col span={4} offset={2}>
                <Button
                  size={"large"}
                  style={{ backgroundColor: "orange" }}
                  onClick={() => {
                    handleSearchStatus({ name: textSearch, activePage : 1 });
                  }}
                >
                  Search
                </Button>
              </Col>
            </Row>
          </Col>

          {/* <Col span={6}>
            <Row>
              <Col span={12}>
                <Button
                  size={"large"}
                  style={{ backgroundColor: "green", color: "white" }}
                  onClick={() => {
                    showModalCreate();
                  }}
                >
                  Create
                </Button>
              </Col>
              <Col span={10} offset={2}>
                <Select size={"large"} placeholder="Sắp xếp"
                  optionFilterProp="children"
                  onChange={onChange}
                  onSearch={onSearch}
                  filterOption={(input, option) =>
                    (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                  }
                  options={[
                    {
                      value: '1',
                      label: 'Từ lớn đến bé',
                    },
                    {
                      value: '1',
                      label: 'Từ bé đến lớn',
                    },
                  ]}
                />
              </Col>
            </Row>
          </Col> */}
        </Row>
      </div>
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {listItems}
        </tbody>
      </table>
      {arr.map((items, key) => {
        return (
          <Button key={key} onClick={() => {
            textSearch ? handleSearchStatus({ name: textSearch, activePage: items }) : handleGetStatus(items)
          }}
            style={{ backgroundColor: activePage === items ? " red" : "white" }}
          >
            {items}
          </Button>
        )
      })}
      <div>
        {/* <Table
          onRow={(record) => {
            return {
              onClick: () => {
                // console.log(record);
              },
            };
          }}
          columns={columns}
          dataSource={listStatus}
          rowKey={(key) => key._id}
        /> */}
      </div>
    </div>
  );
}
